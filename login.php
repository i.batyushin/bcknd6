<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  session_destroy();
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages=array();
  $errors=array();
  $errors['loginpass']=!empty($_COOKIE['loginpass_error']);

  if($errors['loginpass'])
  {
    setcookie('loginpass_error', '', 100000);
    $messages[]='<div class="error"> незалогинилось </div>';
  }
?>
 <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
 </style>
 <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
?>
<a href="admin.php">admin</a>
<form action="" method="post">
 Логин <input name="login" <?php if ($errors['loginpass']) {print 'class="error"';} ?> />
 Пароль<input name="pass" <?php if ($errors['loginpass']) {print 'class="error"';} ?> />
<input type="submit" value="Войти" />
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $errors=false;
  $user = 'u24234';
  $pass = '43523453';
  $db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $log=$db->quote($_POST['login']);
  $passw=md5($_POST['pass']);
  $rez_zap=$db->query("SELECT login FROM loginpass WHERE login=$log");
  foreach ($rez_zap as $elem)
    $login2=$elem['login'];
  $rez_zap=$db->query("SELECT password FROM loginpass WHERE login=$log");
  foreach ($rez_zap as $elem)
    $passw2=$elem['password'];
  // Если все ок, то авторизуем пользователя.
  if(!empty($login2)&&!empty($passw2)&&$passw==$passw2){
    $passw2=$db->quote($passw2);
  $_SESSION['login'] = $_POST['login'];
  $rez_zap=$db->query("SELECT id FROM loginpass WHERE login=$log AND password=$passw2");
  foreach($rez_zap as $elem)
    $id=(int)$elem['id'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $id;
  }
  else {setcookie('loginpass_error', '1', time() + 24 * 60 * 60);
  $errors=true;}
  if($errors){
    header('Location: login.php');
    exit();
  }
  else setcookie('loginpass_error', '', 100000);
  // Делаем перенаправление.
  header('Location: ./');
}
